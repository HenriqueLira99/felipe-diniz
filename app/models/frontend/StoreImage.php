<?php
/**
 * Created by PhpStorm.
 * User: 18digital
 * Date: 30/03/2015
 * Time: 17:25
 */

namespace Frontend;
use \Eloquent;

class StoreImage extends Eloquent{
    protected $table = 'stores_images';

    public function images(){
        return $this->hasMany('\Frontend\StoreImage')->where('featured', '=', 1)->orderBy('order', 'ASC');
    }
}