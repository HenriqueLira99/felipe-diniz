<?php
/**
 * Created by PhpStorm.
 * User: 18digital
 * Date: 30/03/2015
 * Time: 17:25
 */

namespace Frontend;
use \Eloquent;

class StoreType extends Eloquent{
    protected $table = 'stores_types';

    public function images(){
        return $this->hasMany('\Frontend\StoreImage');
    }
}