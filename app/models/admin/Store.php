<?php
/**
 * Created by PhpStorm.
 * User: 18digital
 * Date: 30/03/2015
 * Time: 17:25
 */

namespace Admin;
use \Eloquent;

class Store extends Eloquent{
    protected $table = 'stores';

    public function type(){
        return $this->hasOne('\Admin\StoreType', 'id', 'store_type_id');
    }

    public function images(){
        return $this->hasMany('\Frontend\StoreImage')->where('featured', '=', 1);
    }

}