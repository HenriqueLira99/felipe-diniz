<?php
/**
 * Created by PhpStorm.
 * User: 18digital
 * Date: 30/03/2015
 * Time: 17:25
 */

namespace Admin;
use \Eloquent;

class StoreImage extends Eloquent{
    protected $table = 'stores_images';
}