<?php
/**
 * Created by PhpStorm.
 * User: 18digital
 * Date: 30/03/2015
 * Time: 17:28
 */

namespace Frontend;
use Admin\StoreImage;
use \View;
use \Input;
use \Redirect;
use \Validator;
use \Response;
use \Str;
use \File;
use \DB;

class StoresController extends \BaseController{
    public function index(){
        //show homepage
        $stores = Store::where('featured', '=', '1')->get();

        return View::make('frontend.index', compact('stores'));
    }

    public function getImovel($storeAlias = NULL, $storeId = NULL){
        //show homepage
        $store       = Store::where('id', '=', $storeId)->where('alias', '=', $storeAlias)->where('status', '=', '1')->first();
        $storeImages = $store->images;

        return View::make('frontend.store.index', compact('store', 'storeImages'));
    }
}