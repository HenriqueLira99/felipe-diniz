<?php
/**
 * Created by PhpStorm.
 * User: 18digital
 * Date: 30/03/2015
 * Time: 17:28
 */

namespace Frontend;
use \View;
use \Input;
use \Redirect;
use \Validator;
use \Response;
use \Str;
use \DB;

class StoresTypesController extends \BaseController{
    public function getIndex($type = NULL){
        $storeType = StoreType::where('alias', '=', $type)->first();

        //show homepage
        $stores = Store::where('store_type_id', '=', $storeType->id)->where('status', '=', 1)->get();

        //$store       = Store::where('id', '=', $storeId)->where('alias', '=', $storeAlias)->first();
        //$storeImages = StoreImage::where('store_id', '=', $store->id)->get();

        //echo '<pre>';
        //    var_dump(DB::getQueryLog());
        //echo '</pre>';

        return View::make('frontend.storetype.index', compact('stores', 'storeType'));
    }
}