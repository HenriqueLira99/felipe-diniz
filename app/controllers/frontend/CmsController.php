<?php
/**
 * Created by PhpStorm.
 * User: 18digital
 * Date: 30/03/2015
 * Time: 17:28
 */

namespace Frontend;
use \View;
use \Input;
use \Redirect;

class CmsController extends \BaseController{
    public function getBiografia(){
        //show homepage
        $cms = Cms::where('code', '=', 1)->first();

        return View::make('frontend.cms.index', compact('cms'));
    }

    public function getContato(){
        //show homepage
        $cms = Cms::where('code', '=', 2)->first();

        return View::make('frontend.cms.index', compact('cms'));
    }
}