<?php
/**
 * Created by PhpStorm.
 * User: 18digital
 * Date: 30/03/2015
 * Time: 17:28
 */

namespace Admin;
use \View;
use \Input;
use \Redirect;
use \Validator;
use \Response;
use \Str;

class StoresTypesController extends \BaseController{
    public function __construct(){
        $this->beforeFilter('csrf', ['on' => 'post']);
    }

    public function getIndex(){
        //show homepage
        $storesTypes = StoreType::all();

        return View::make('admin.storetype.index', compact('storesTypes'));
    }

    public function getCreate(){
        // Show the create game form.
        return View::make('admin.storetype.create');
    }

    public function postHandleCreate(){
        // Handle create form submission.
        $messages = array(
            'required' => 'O campo :attribute é obrigatório.',
        );

        $validator = Validator::make(Input::all(), [
            'nome'      => 'required',
            'status'    => 'required'
        ], $messages);

        if( $validator->fails() ) return Redirect::action('\Admin\StoresTypesController@getCreate')->withErrors($validator);

        $storeType             = new StoreType;
        $storeType->name       = Input::get('nome');
        $storeType->status     = Input::get('status');
        $storeType->save();

        return Redirect::action('\Admin\StoresTypesController@getIndex');
    }

    public function getEdit($id){
        // Show the edit game form.
        $storeType = StoreType::find($id);

        return View::make('admin.storetype.edit', compact('storeType'));
    }

    public function getHandleEdit(){
        // Handle edit form submission.
    }

    public function postHandleEdit(){
        // Handle edit form submission.
        $storeType              = StoreType::findOrFail(Input::get('id'));
        $storeType->name        = Input::get('name');
        $storeType->status      = Input::get('status');
        $storeType->save();

        return Redirect::action('\Admin\StoresTypesController@getIndex');
    }

     public function getDelete($id){
        // Show delete confirmation page.
        $storeType = StoreType::find($id);
        //var_dump($store);
        //exit;

        if(empty($storeType)){
            return Redirect::action('\Admin\StoresTypesController@getIndex')->withErrors(array('erro' => 'ID desconhecida'));
        }

        return View::make('admin.storetype.delete', compact('storeType'));
     }

     public function postDelete(){
         // Handle the delete confirmation.
         $id         = Input::get('id');
         $storeType  = StoreType::findOrFail($id);
         $storeType->delete();

         return Redirect::action('\Admin\StoresTypesController@getIndex');
     }
}