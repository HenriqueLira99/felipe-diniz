<?php
/**
 * Created by PhpStorm.
 * User: 18digital
 * Date: 30/03/2015
 * Time: 17:28
 */

namespace Admin;
use \View;
use \Input;
use \Redirect;


class ContactsController extends \BaseController{
    public function __construct(){
        $this->beforeFilter('csrf', ['on' => 'post']);
    }

    public function getIndex(){
        //show homepage
        $contact = Contact::where('code', '=', 2)->first();

        return View::make('admin.contact.index', compact('contact'));
    }

     public function getCreate(){
         // Show the create game form.
         return View::make('admin.contact.create');
     }

     public function getHandleCreate(){
         // Handle create form submission.
         return Redirect::action('ContactController@index');
     }

     public function getEdit(){
         $contact = Contact::where('code', '=', 2)->first();
         // Show the edit game form.
         return View::make('admin.contact.edit', compact('contact'));
     }

    public function getHandleEdit(){
     // Handle edit form submission.
    }

    public function postHandleEdit(){
        // Handle edit form submission.
        $contact              = Contact::findOrFail(Input::get('id'));
        $contact->text        = Input::get('text');
        $contact->save();

        return Redirect::action('\Admin\ContactsController@getIndex');
    }


    public function getDelete(Contacts $contacts){
         // Show delete confirmation page.
         return View::make('admin.contact.delete', compact('contacts'));
     }

     public function getHandleDelete(){
         // Handle the delete confirmation.
     }
}