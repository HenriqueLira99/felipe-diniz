<?php
/**
 * Created by PhpStorm.
 * User: 18digital
 * Date: 30/03/2015
 * Time: 17:28
 */

namespace Admin;
use \View;
use \Input;
use \Redirect;

class BiographiesController extends \BaseController{
    public function __construct(){
        $this->beforeFilter('csrf', ['on' => 'post']);
    }

    public function getIndex(){
        //show homepage
        $biography = Biography::where('code', '=', 1)->first();

        return View::make('admin.biography.index', compact('biography'));
    }

     public function getCreate(){
     // Show the create game form.
         return View::make('admin.biography.create');
     }

     public function getHandleCreate(){
         // Handle create form submission.
         return Redirect::action('\Admin\BiographyController@index');
     }

     public function getEdit(){
         $biography = Biography::where('code', '=', 1)->first();
         // Show the edit game form.
         return View::make('admin.biography.edit', compact('biography'));
     }

    public function getHandleEdit(){
    // Handle edit form submission.
    }

    public function postHandleEdit(){
     // Handle edit form submission.
         $biography              = Biography::findOrFail(Input::get('id'));
         $biography->text        = Input::get('text');
         $biography->save();

        return Redirect::action('\Admin\BiographiesController@getIndex');
    }

    public function getDelete(Biographies $biographies){
     // Show delete confirmation page.
         return View::make('admin.biography.delete', compact('biographies'));
    }

    public function getHandleDelete(){
     // Handle the delete confirmation.
    }
}