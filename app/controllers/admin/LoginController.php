<?php
namespace Admin;
use \View;
use \Auth;
use \Validator;
use \Redirect;
use \Input;

class LoginController extends \Controller{
	public function __construct(){
		$this->beforeFilter('csrf', ['on' => 'post']);
	}

	public function getLogin(){
		return View::make('admin.security.login');
	}

	public function postLogin(){
		$validator = Validator::make(Input::all(), [
			'username' => 'required',
			'password' => 'required'
		]);

		if( $validator->fails() ) return Redirect::to('admin/login');

		if( Auth::attempt(['username' => Input::get('username'), 'password' => Input::get('password')]) ) {
            return Redirect::intended('admin/');
        }else {
            return Redirect::intended('admin/login')->withErrors(array('error' => 'Login ou senha incorretos'));
        }
	}

    public function getLogout(){
        Auth::logout();

        return Redirect::action('\Admin\LoginController@getLogin');
    }

}
