<?php
/**
 * Created by PhpStorm.
 * User: 18digital
 * Date: 30/03/2015
 * Time: 17:28
 */

namespace Admin;
use \View;
use \Input;
use \Redirect;
use \Str;

class BannersController extends \BaseController{
    public function __construct(){
        $this->beforeFilter('csrf', ['on' => 'post']);
    }

    public function getIndex(){
        //show homepage
        $banner = Banner::first();

        return View::make('admin.banners.index', compact('banner'));
    }

     public function getCreate(){
         // Show the create game form.
         return View::make('admin.banners.create');
     }

     public function getHandleCreate(){
         // Handle create form submission.
         return Redirect::action('\Admin\BannersController@index');
     }

     public function getEdit(){
         // Show the edit game form.
         $banner = Banner::first();

         return View::make('admin.banners.edit', compact('banner'));
     }

     public function getHandleEdit(){
         // Handle edit form submission.
     }


    public function postHandleEdit(){
        // Handle edit form submission.
        $name = Str::slug(str_replace('.'.Input::file('image')->getClientOriginalExtension(), '', Input::file('image')->getClientOriginalName())).'.'.Input::file('image')->getClientOriginalExtension();

        Input::file('image')->move('public/images/banners', $name);

        $banner              = Banner::findOrFail(Input::get('id'));
        $banner->image       = $name;
        $banner->save();

        return Redirect::action('\Admin\BannersController@getIndex');
    }


    public function getDelete(Banners $banners){
        // Show delete confirmation page.
        return View::make('admin.banners.delete', compact('banners'));
    }

    public function getHandleDelete(){
        // Handle the delete confirmation.
    }
}