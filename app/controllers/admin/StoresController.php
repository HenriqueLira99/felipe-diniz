<?php
/**
 * Created by PhpStorm.
 * User: 18digital
 * Date: 30/03/2015
 * Time: 17:28
 */

namespace Admin;
use \View;
use \Input;
use \Redirect;
use \Validator;
use \Response;
use \Str;
use \File;

class StoresController extends \BaseController{
    public function __construct(){
        $this->beforeFilter('csrf', ['on' => 'post']);
    }

    public function index(){
        //show homepage
        $stores = Store::all();

        return View::make('admin.index', compact('stores'));
    }

    public function getCreate(){
        // Show the create game form.
        $storesTypes = StoreType::all();
        return View::make('admin.create', compact('storesTypes'));
    }

    public function postHandleCreate(){
        // Handle create form submission.
        $messages = array(
            'required' => 'O campo :attribute é obrigatório.',
        );

        $validator = Validator::make(Input::all(), [
            'nome'      => 'required',
            'status'    => 'required',
            'tipo'      => 'required',
            'destaque'  => 'required'
        ], $messages);

        if( $validator->fails() ) return Redirect::action('\Admin\StoresController@getCreate')->withErrors($validator);

        $store                 = new Store;
        $store->name           = Input::get('nome');
        $store->alias          = Str::slug(Input::get('nome'));
        $store->store_type_id  = Input::get('tipo');
        $store->status         = Input::get('status');
        $store->featured       = Input::get('destaque');
        $store->save();

        return Redirect::action('\Admin\StoresController@index');
    }

    public function getEdit($id){
        // Show the edit game form.
        $store          = Store::find($id);
        $storesTypes    = StoreType::all();
        $storeImages    = StoreImage::where('store_id', $id)->get();

        return View::make('admin.edit', compact('store', 'storesTypes', 'storeImages'));
    }

    public function getHandleEdit(){
        // Handle edit form submission.
    }

    public function postHandleEdit(){
        // Handle edit form submission.
        $store                 = Store::findOrFail(Input::get('id'));
        $store->name           = Input::get('name');
        $store->alias          = Str::slug(Input::get('name'));
        $store->store_type_id  = Input::get('store_type_id');
        $store->status         = Input::get('status');
        $store->featured       = Input::get('featured_store');
        $store->save();
        
		foreach(Input::get('sort_order', array()) as $key => $value){
			StoreImage::where('store_id', '=', Input::get('id'))
				->where('id', '=', $key)
				->update(array('order' => $value));
		}

        // Update "featured" column values
        StoreImage::where('store_id', '=', Input::get('id'))->update(array('featured' => 0));
        foreach(Input::get('featured', array()) as $key => $value){
            StoreImage::where('store_id', '=', Input::get('id'))
                ->where('id', '=', $key)
                ->update(array('featured' => $value));
        }

        return Redirect::action('\Admin\StoresController@index');
    }

    public function postUpload(){
        $rules = [
            'file' => 'required|max:50',
            'store_id' => 'required'
        ];

        $validator = Validator::make(Input::all(), $rules);

        if( $validator->fails() ) return App::abort(403);

        $files = Input::file('file');
        $response = FALSE;
        $responseImages = array();
        foreach( $files as $file ){
            $filepath = '/images/imoveis';
            $fullFilepath = public_path().$filepath;
            $last_name = md5(rand(0, 99999).date('dmYHisu'));
            $filename = Str::slug(str_replace('.'.$file->getClientOriginalExtension(), '', $file->getClientOriginalName())).$last_name.'.'.$file->getClientOriginalExtension();

            // Move image
            $response = $file->move($fullFilepath, $filename);

            // Save image to product
            $image = new StoreImage;
            $image->store_id = Input::get('store_id');
            $image->path = $filepath.'/'.$filename;
            $image->created_at = date('Y-m-d H:i:s');
            $image->save();

            $responseImages[] = array('filepath' => $image->path, 'id' => $image->id, 'name' => $filename);

            /*$image = StoreImage::insert([
                'store_id' => Input::get('store_id'),
                'path' => $filepath.'/'.$filename,
                'created_at' => date('Y-m-d H:i:s')
            ]);*/
        }

        if( $response ) return $responseImages;
        else return Response::json('error', 400);
    }

    public function postRemoveImage(){
        $rules = [
            'id' => 'required',
            'store_id' => 'required',
        ];

        $validator = Validator::make(Input::all(), $rules);

        if( $validator->fails() ) return Response::json('error', 200);

        $id = Input::get('store_id');
        $image = StoreImage::find(Input::get('id'));
        $filepath = str_replace(['/','\\'], DIRECTORY_SEPARATOR, public_path().'/'.$image->path);

        File::delete($filepath);
        $image->delete();

        return Response::json('success', 200);
    }

    public function getDelete($id){
       // Show delete confirmation page.
       $store = Store::find($id);
       //var_dump($store);
       //exit;

       if(empty($store)){
           return Redirect::action('\Admin\StoreController@index')->withErrors(array('erro' => 'ID desconhecida'));
       }

       return View::make('admin.delete', compact('store'));
    }

    public function postDelete(){
        // Handle the delete confirmation.
        $id     = Input::get('store');
        $store  = Store::findOrFail($id);
        $store->delete();

        return Redirect::action('\Admin\StoresController@index');
    }
}