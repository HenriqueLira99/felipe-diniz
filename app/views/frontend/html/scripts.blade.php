<script type="text/javascript" src="{{URL::asset('js/jquery.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/bootstrap/dist/js/bootstrap.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/masonry.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/imagesloaded.pkgd.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/jquery.sidr.min.js')}}"></script>

<script>
	jQuery(document).ready(function() {
		jQuery('#simple-menu').sidr({
			side: 'right'
		});
		jQuery('.close-sidr').on('click', function(){
			jQuery.sidr('close');
		});
	});
</script>