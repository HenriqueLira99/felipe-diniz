<div class="menu-holder">
	<div class="hidden-xs">
		<div class="logo pull-left">
			<a href="{{URL::to('/')}}" style="color:#112D34; font-family: misoregular;font-size: 26px;">Felipe Diniz Arquitetura</a>
		</div>
		<div class="pull-right">
		    <ul class="list-unstyled list-inline">
		        @foreach(\Frontend\StoreType::all() as $menu)
		            <li><a href="{{URL::to($menu->alias)}}" class="{{{ (Request::is($menu->alias) ? 'active' : '') }}}">{{$menu->name}}</a></li>
		        @endforeach
		        <li><a href="{{'biografia'}}" class="{{{ (Request::is('biografia') ? 'active' : '') }}}">Bio</a></li>
		        <li><a href="{{'contato'}}" class="{{{ (Request::is('contato') ? 'active' : '') }}}">Contato</a></li>
		    </ul>
		</div>
	</div>
	<div class="visible-xs">
		<div class="logo pull-left">
			<a href="{{URL::to('/')}}" style="color:#112D34; font-family: misoregular;font-size: 24px;">Felipe Diniz Arquitetura</a>
		</div>
		<button type="button" id="simple-menu" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
			<img src="{{URL::asset('images/icon_bar.png')}}" />
	        <!-- <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>-->
	    </button>
	    <div class="clearfix"></div>
	   	<div id="sidr" class="pull-right navbar-collapse collapse">
			<div class="close-btn">
				<!-- <a href="#" class="close-sidr"><img src="{{URL::asset('images/icon_close.png')}}"/></a> -->
				<!-- <a href="#" class="close-sidr">+</a> -->
			</div>
		    <ul class="list-unstyled list-inline navbar-nav nav">
		        @foreach(\Frontend\StoreType::all() as $menu)
		            <li><a href="{{URL::to($menu->alias)}}" class="{{{ (Request::is($menu->alias) ? 'active' : '') }}}">{{$menu->name}}</a></li>
		        @endforeach
		        <li><a href="{{'biografia'}}" class="{{{ (Request::is('biografia') ? 'active' : '') }}}">Bio</a></li>
		        <li><a href="{{'contato'}}" class="{{{ (Request::is('contato') ? 'active' : '') }}}">Contato</a></li>
		    </ul>
			<p style="position: absolute; bottom: 8px; left: 11px; color: #FFF; font-size: 16px; font-family: misoregular;">© Felipe Diniz</p>
		</div>
	</div>
</div>