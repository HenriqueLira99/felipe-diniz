<link rel="shortcut icon" href="images/icon.png">
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700,800' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Raleway:300,200,100' rel='stylesheet' type='text/css'>

<link href="{{URL::asset('js/bootstrap/dist/css/bootstrap.css')}}" rel="stylesheet">
<link rel="stylesheet" href="{{URL::asset('fonts/font-awesome-4/css/font-awesome.min.css')}}">
<link href="{{URL::asset('css/style.css')}}" rel="stylesheet" />
<link href="{{URL::asset('css/jquery.sidr.light.css')}}" rel="stylesheet" />