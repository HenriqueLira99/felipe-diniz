<div class="pull-right">
	<div class="menu-holder">
		<div class="visible-xs">
			<div class="logo pull-left">
				<a href="{{URL::to('/')}}"><img src="{{URL::asset('images/felipe_default_logo.png')}}" /></a>
			</div>
			<!-- <nav class="navbar navbar-default navbar-fixed-top" role="navigation"> -->
				<!-- <div class="navbar-header"> -->
					<button type="button" id="simple-menu" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
			<!-- 	</div> -->
			<!-- </nav> -->
			<div class="clearfix"></div>
			<div id="sidr" class="pull-right navbar-collapse collapse">
				<ul class="list-unstyled list-inline navbar-nav nav">
					@foreach(\Frontend\StoreType::all() as $menu)
						<li><a href="{{URL::to($menu->alias)}}" class="{{{ (Request::is($menu->alias) ? 'active' : '') }}}">{{$menu->name}}</a></li>
					@endforeach
					<li><a href="{{'biografia'}}" class="{{{ (Request::is('biografia') ? 'active' : '') }}}">Bio</a></li>
					<li><a href="{{'contato'}}" class="{{{ (Request::is('contato') ? 'active' : '') }}}">Contato</a></li>
				</ul>
			</div>
		</div>
	</div>
</div>