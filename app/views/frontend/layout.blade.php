<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>@yield('title')</title>
        @yield('before_head')
        @include('frontend.html.head')
        @yield('head')
    </head>
    <body class="animate">
        @if(!isset($_COOKIE['overlay_cookie']))
            <div id="overLayClick"></div>
            <div id="overLay" style="-webkit-transform: translateX(-20px);" class="">
                <div id="phrase">
                    <p><img src="{{URL::asset('images/banners/capa_logo.png')}}" style="max-width: 50px" /></p>
                </div>
                <div id="overLayImage"><img width="1500" height="1209" src="{{URL::asset('images/banners/'.\Frontend\Store::getCapa()->image)}}" ></div>
                <div id="overLayBar">&nbsp;</div>
            </div>
        @elseif(isset($_COOKIE['overlay_cookie']))
            <div id="overLayClick"></div>
            <div id="overLay" style="-webkit-transform: translateX(100%);" class="">
                <div id="phrase">
                    <p><img src="{{URL::asset('images/banners/capa_logo.png')}}" style="max-width: 50px" /></p>
                </div>
                <div id="overLayImage"><img width="1500" height="1209" src="{{URL::asset('images/banners/'.\Frontend\Store::getCapa()->image)}}" ></div>
                <div id="overLayBar">&nbsp;</div>
            </div>
        @endif
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="navbar-header">
                <div class="container">
                    @include('frontend.html.menu')
                    @yield('menu')
                </div>
            </div>
        </nav>
		{{-- @include('frontend.html.menumobile') --}}
        <div class="container">
            @yield('content')
        </div>
        @include('frontend.html.scripts')
        @yield('scripts')
        <script type="text/javascript" src="{{URL::asset('js/cookies.js')}}"></script>
        <script type="text/javascript">
            $(window).resize(function () {
                $('body').css('padding-top', parseInt($('.navbar').css("height"))+20);
            });

            $(window).load(function () {
                $('body').css('padding-top', parseInt($('.navbar').css("height"))+20);
            });
        </script>
        <script type="text/javascript" src="{{URL::asset('js/magic_mantle.js')}}"></script>
    </body>
</html>