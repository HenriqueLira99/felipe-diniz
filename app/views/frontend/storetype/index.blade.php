{{-- call the layout.blade.php --}}
@extends('frontend.layout')
{{-- add the content in layout --}}
@section('title')
    {{'Felipe Diniz - '.$storeType->name}}
@stop
@section('content')
    <div class="row box-holder">
        @foreach($stores as $store)
            @foreach($store->featuredImages as $image)
                <div class="col-md-4 col-xs-12 item">
                    <a href="imovel/{{$store->alias}}/{{$store->id}}" class="box">
                        <div class="img-box">
                            <div class="hover">
                                <span class="vertical-align-reference"></span>
                                <p class="text-center vertical-align">{{$store->name}}</p>
                            </div>
                            <img src="{{URL::asset($image->path)}}" class="img-responsive" style="display: none" />
                        </div>
                    </a>
                </div>
            @endforeach
        @endforeach
    </div>
@stop
@section('scripts')
	<script type="text/javascript" src="{{URL::asset('js/afterimageload.js')}}"></script>
    <script type="text/javascript">
        jQuery('.item').hover(function(el){
            if(el.type == 'mouseenter'){
                jQuery(el.currentTarget).find('.hover').stop().fadeIn(600);
            }else if(el.type == 'mouseleave'){
                jQuery(el.currentTarget).find('.hover').stop().fadeOut(600);
            }
        });
    </script>
@stop