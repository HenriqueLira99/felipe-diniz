<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{'Felipe Diniz - '.$store->name}}</title>
        @include('frontend.html.head')
    </head>
    <body class="animated store-view">
        @if(!isset($_COOKIE['overlay_cookie']))
            <div id="overLayClick"></div>
            <div id="overLay" style="-webkit-transform: translateX(-20px);" class="">
                <div id="phrase">
                    <p><img src="{{URL::asset('images/banners/capa_logo.png')}}" style="max-width: 50px" /></p>
                </div>
                <div id="overLayImage"><img width="1500" height="1209" src="{{URL::asset('images/banners/'.\Frontend\Store::getCapa()->image)}}" ></div>
                <div id="overLayBar">&nbsp;</div>
            </div>
        @elseif(isset($_COOKIE['overlay_cookie']))
            <div id="overLayClick"></div>
            <div id="overLay" style="-webkit-transform: translateX(100%);" class="">
                <div id="phrase">
                    <p><img src="{{URL::asset('images/banners/capa_logo.png')}}" style="max-width: 50px" /></p>
                </div>
                <div id="overLayImage"><img width="1500" height="1209" src="{{URL::asset('images/banners/'.\Frontend\Store::getCapa()->image)}}" ></div>
                <div id="overLayBar">&nbsp;</div>
            </div>
        @endif
        <nav class="navbar navbar-default" role="navigation">
            <div class="navbar-header">
                <div class="container">
                    <div class="menu-holder">
                        <div class="hidden-xs">
                            <div class="logo pull-left">
                                <a href="{{URL::to('/')}}" style="color:#112D34; font-family: misoregular;font-size: 26px;">Felipe Diniz Arquitetura</a>
                            </div>
                            <div class="pull-right">
                                <div class="close-btn">
                                    <a href="{{URL::previous()}}">Close</a>
                                </div>
                            </div>
                        </div>
                        <div class="visible-xs">
                            <div class="logo pull-left">
								<a href="{{URL::to('/')}}" style="color:#112D34; font-family: misoregular;font-size: 24px;">Felipe Diniz Arquitetura</a>
                            </div>
                            <div class="pull-right">
                                <div class="close-btn">
                                    <a href="{{URL::previous()}}"><img src="{{URL::asset('images/icon_close.png')}}"/></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
        <div class="container slider-container">
            <div class="row box-holder image-slider">
                <div id="carousel" class="carousel slide carousel-fade" data-ride="carousel">
                    <ol class="carousel-indicators">
                        @for($i = 0; $i <= count($storeImages); $i++)
                            @if($i == 0)
                                <li data-target="#carousel" data-slide-to="{{$i}}" class="active"></li>
                            @else
                                <li data-target="#carousel" data-slide-to="{{$i}}"></li>
                            @endif
                        @endfor
                    </ol>
                    <div class="carousel-inner">
                        <?php $count = 0 ?>
                        @foreach($storeImages as $storeImage)
                            <?php
                                $path = public_path().$storeImage->path;
                                $path = str_replace(array('/','\\'), DIRECTORY_SEPARATOR, $path);
                                list($width, $height) = getimagesize($path);
                                if($width >= $height){
                                    $position = '-webkit-background-size: auto !important;-moz-background-size: auto !important;-o-background-size: auto !important;background-size: auto !important;';
                                }else{
                                    $position = '-webkit-background-size: auto !important;-moz-background-size: auto !important;-o-background-size: auto !important;background-size: auto !important;';
                                }
                            ?>
                            @if($count == 0)
                                <div class="active item">
                                    <img class="vertical-align-img" src="{{URL::asset($storeImage->path)}}"/>
                                </div>
                            @else
                                <div class="item">
                                    <img class="vertical-align-img" src="{{URL::asset($storeImage->path)}}"/>
                                </div>
                            @endif
                            <?php $count++ ?>
                        @endforeach
                    </div>
                    <!-- Carousel nav -->
                    <a class="carousel-control left" href="#carousel" data-slide="prev">&lsaquo;</a>
                    <a class="carousel-control right" href="#carousel" data-slide="next">&rsaquo;</a>
                </div>
            </div>
            <div class="row footer-slider">
                <div class="col-xs-12" style="height: 100%;">
                    <div class="row box-holder" style="height: 100%;">
                        <div class="col-xs-12" style="height: 100%;">
                            <div class="" style="height: 100%;">
                                <span class="vertical-align-reference"></span>
                                <div class="footer-holder vertical-align">
                                    <div class="pull-left">
                                        <p class="">{{$store->name}}</p>
                                        <p class="current-and-active"></p>
                                    </div>
                                    <div class="logo-copyright pull-right">
                                        <a href="{{URL::to('/')}}" style="margin-top: 25px;float: left;color: #112D34; font-size:16px; font-family: misoregular;" class="visible-xs">© Felipe Diniz</a>
                                        <a href="{{URL::to('/')}}" style="margin-top: 25px;float: left;color: #112D34; font-size:19px; font-family: misoregular;" class="hidden-xs">© Felipe Diniz</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        @include('frontend.html.scripts')
        <script type="text/javascript">
			// jQuery(document).ready(function(){
				// var sizes = '';
				// sizes += 'Width da imagem: '+jQuery('.vertical-align-img').first().height();
				// sizes += '\n Height da imagem: '+jQuery('.vertical-align-img').first().width();
				// sizes += '\n Width da box: '+jQuery('.active.item').first().width();
				// sizes += '\n Height da box: '+jQuery('.active.item').first().height();
				// alert(sizes);
			// });
			// jQuery('.vertical-align-img').first().load(function(){
				// var sizesimg = '';
				// sizesimg += 'Width da imagem: '+jQuery('.vertical-align-img').first().height();
				// sizesimg += '\n Height da imagem: '+jQuery('.vertical-align-img').first().width();
				// alert(sizesimg);
			// });
			$('.carousel').carousel({
				interval: false
			});

            var totalItems = $('.item').length;
            var currentIndex = $('div.active').index() + 1;
            $('.current-and-active').html('Photo '+currentIndex+' of '+totalItems+'');

			$('.carousel').on('slid.bs.carousel', function () {
				var carouselData = $(this).data('bs.carousel');
				var currentIndex = carouselData.getItemIndex(carouselData.$element.find('.item.active')) + 1;
				var total = carouselData.$items.length;

				$('.current-and-active').html('Photo '+currentIndex+' of '+total+'');
			});

			$("body").keydown(function(e) {
				if(e.keyCode == 37) { // left
					$('.carousel').carousel('prev');
				} else if(e.keyCode == 39) { // right
					$('.carousel').carousel('next');
				}
			});

            $(document).ready(function() {
                $('.active.item .fill').first().fadeIn(600);
            });
       </script>
       <script type="text/javascript" src="{{URL::asset('js/cookies.js')}}"></script>
       <script type="text/javascript" src="{{URL::asset('js/magic_mantle.js')}}"></script>
    </body>
</html>