{{-- call the layout.blade.php --}}
@extends('frontend.layout')
{{-- add the content in layout --}}
@section('title')
    {{'Felipe Diniz - '.$cms->page_name}}
@stop
@section('content')
    <div class="row">
        <div class="col-md-12 {{$cms->alias}} cms">
            {{$cms->text}}
        </div>
    </div>
@stop
@section('scripts')
    <script type="text/javascript">
        jQuery(document).ready(function(){
            jQuery('.cms').hide();
            jQuery('.cms').each(function(i) {
                jQuery(this).fadeIn(600);
            });
        });
    </script>
@stop