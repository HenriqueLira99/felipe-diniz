<script type="text/javascript" src="{{URL::asset('js/jquery.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/jquery.cookie/jquery.cookie.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/jquery.pushmenu/js/jPushMenu.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/jquery.nanoscroller/jquery.nanoscroller.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/jquery.sparkline/jquery.sparkline.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/jquery.ui/jquery-ui.js')}}" ></script>
<script type="text/javascript" src="{{URL::asset('js/jquery.gritter/js/jquery.gritter.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/behaviour/core.js')}}"></script>

<script type="text/javascript" src="{{URL::asset('js/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/behaviour/dashboard.js')}}"></script>
<!--
<script type="text/javascript" src="{{URL::asset('js/jquery.flot/jquery.flot.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/jquery.flot/jquery.flot.pie.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/jquery.flot/jquery.flot.resize.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/jquery.flot/jquery.flot.labels.js')}}"></script>
-->