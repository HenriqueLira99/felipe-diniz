<link rel="shortcut icon" href="images/icon.png">
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700,800' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Raleway:300,200,100' rel='stylesheet' type='text/css'>

<link href="{{URL::asset('js/bootstrap/dist/css/bootstrap.css')}}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{URL::asset('js/jquery.gritter/css/jquery.gritter.css')}}" />
<link rel="stylesheet" href="{{URL::asset('fonts/font-awesome-4/css/font-awesome.min.css')}}">

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="{{URL::asset('js/html5shiv.js')}}"></script>
<script src="{{URL::asset('js/respond.min.js')}}"></script>
<![endif]-->
<link rel="stylesheet" type="text/css" href="{{URL::asset('js/jquery.nanoscroller/nanoscroller.css')}}" />

<link rel="stylesheet" type="text/css" href="{{URL::asset('js/jquery.codemirror/lib/codemirror.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::asset('js/jquery.codemirror/theme/ambiance.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::asset('js/jquery.vectormaps/jquery-jvectormap-1.2.2.css')}}"  media="screen"/>

<link href="{{URL::asset('css/admin_style.css')}}" rel="stylesheet" />