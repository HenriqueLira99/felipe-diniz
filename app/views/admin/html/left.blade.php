<div class="cl-sidebar">
	<div class="cl-toggle"><i class="fa fa-bars"></i></div>
	<div class="cl-navblock">
		<div class="menu-space">
			<div class="content">
				<div class="sidebar-logo">
					<div class="logo"></div>
				</div>
				<ul class="cl-vnavigation">
					<li><a href="{{action('\Admin\StoresController@index')}}"><i class="fa fa-home"></i><span>Imóveis</span></a>
						<ul class="sub-menu">
							<li><a href="{{action('\Admin\StoresController@index')}}"><i class="fa fa-align-justify"></i>Ver todos</a></li>
							<li><a href="{{action('\Admin\StoresController@getCreate')}}"><i class="fa fa-plus-square"></i>Adicionar</a></li>
							<li><a href="{{action('\Admin\StoresTypesController@getIndex')}}"><i class="fa fa-align-justify"></i>Ver todos tipos</a></li>
							<li><a href="{{action('\Admin\StoresTypesController@getCreate')}}"><i class="fa fa-plus-square"></i>Adicionar tipo de imóvel</a></li>
						</ul>
					</li>
					<li><a href="{{action('\Admin\BiographiesController@getIndex')}}"><i class="fa fa-smile-o"></i><span>Biografia</span></a>
						<ul class="sub-menu">
						    <li><a href="{{action('\Admin\BiographiesController@getIndex')}}"><i class="fa fa-desktop"></i><span>Visualizar</span></a></li>
							<li><a href="{{action('\Admin\BiographiesController@getEdit', array('type' => 'bio'))}}"><i class="fa fa-edit"></i>Editar</a></li>
						</ul>
					</li>
					<li><a href="{{action('\Admin\ContactsController@getIndex')}}"><i class="fa fa-list-alt"></i><span>Contato</span></a>
						<ul class="sub-menu">
						    <li><a href="{{action('\Admin\ContactsController@getIndex')}}"><i class="fa fa-desktop"></i><span>Visualizar</span></a></li>
							<li><a href="{{action('\Admin\ContactsController@getEdit', array('type' => 'contact'))}}"><i class="fa fa-edit"></i>Editar</a></li>
						</ul>
					</li>
					<li><a href="{{action('\Admin\BannersController@getIndex')}}"><i class="fa fa-desktop"></i><span>Capa</span></a>
						<ul class="sub-menu">
							<li><a href="{{action('\Admin\BannersController@getIndex')}}"><i class="fa fa-align-justify"></i>Ver</a></li>
							<li><a href="{{action('\Admin\BannersController@getEdit')}}"><i class="fa fa-edit"></i>Alterar</a></li>
						</ul>
					</li>
					<li><a href="{{action('\Admin\LoginController@getLogout')}}"><i class="fa fa-sign-out"></i><span>Logout</span></a></li>
				</ul>
			</div>
		</div>
	</div>
</div>