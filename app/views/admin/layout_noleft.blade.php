<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Felipe Diniz - Administração</title>
    @yield('before_head')
    @include('admin.html.head')
    @yield('head')
</head>
<body class="animated">
    <div id="cl-wrapper">
        <div class="container">
            @yield('content')
            @include('admin.html.scripts')
            @yield('scripts')
        </div>
    </div>
</body>
</html>