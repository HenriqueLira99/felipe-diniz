@extends('admin.layout')

@section('content')
    <div class="page-header">
        <h1>Cadastre um novo Imóvel <small></small></h1>
    </div>
    <div class="block-flat">
        <form action="{{action('\Admin\StoresController@postHandleCreate')}}" method="post" role="form">
            {{Form::token()}}
            <div class="form-group">
                <label>Nome</label>
                @foreach($errors->get('nome') as $message)
                    <div class="alert alert-warning">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <i class="fa fa-warning sign"></i><strong>Atenção!</strong> {{$message}}
                    </div>
                @endforeach
                <input type="text" placeholder="Digite o nome" class="form-control" name="nome" required="required">
            </div>
            <div class="form-group">
                <label>Tipo</label>
                @foreach($errors->get('tipo') as $message)
                    <div class="alert alert-warning">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <i class="fa fa-warning sign"></i><strong>Atenção!</strong> {{$message}}
                    </div>
                @endforeach
                <select class="form-control" name="tipo" required="required">
                    <option value="">Selecione...</option>
                    @foreach($storesTypes as $storeType)
                        <option value="{{$storeType->id}}">{{$storeType->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label>Status</label>
                @foreach($errors->get('status') as $message)
                    <div class="alert alert-warning">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <i class="fa fa-warning sign"></i><strong>Atenção!</strong> {{$message}}
                    </div>
                @endforeach
                <select class="form-control" name="status" required="required">
                    <option value="">Selecione...</option>
                    <option value="0">Desativado</option>
                    <option value="1">Ativado</option>
                </select>
            </div>
            <div class="form-group">
                <label>Destaque na home?</label>
                @foreach($errors->get('destaque') as $message)
                    <div class="alert alert-warning">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <i class="fa fa-warning sign"></i><strong>Atenção!</strong> {{$message}}
                    </div>
                @endforeach
                <select class="form-control" name="destaque" required="required">
                    <option>Selecione...</option>
                    <option value="0">Não</option>
                    <option value="1">Sim</option>
                </select>
            </div>
            <button class="btn btn-primary" type="submit">Cadastrar</button>
            <a href="{{action('\Admin\StoresController@index')}}"><button class="btn btn-default" type="button" >Cancelar</button></a>
        </form>
    </div>
@stop