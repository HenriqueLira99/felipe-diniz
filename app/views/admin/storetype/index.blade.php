{{-- call the layout.blade.php --}}
@extends('admin.layout')
{{-- add the content in layout --}}
@section('content')
    <div class="page-header">
        <h1>Todos tipos <small>cadastrados</small></h1>
    </div>

    <div class="panel panel-default">
        <div class="panel-body">
            <a href="{{ action('\Admin\StoresTypesController@getCreate') }}" class="btn btn-primary">Criar novo</a>
        </div>
    </div>

    @if ($storesTypes->isEmpty())
        <p>Nenhuma entrada encontrada! :(</p>
    @else
        <div class="block-flat">
            <table class="table no-border hover">
                <thead class="no-border">
                    <tr>
                        <th>Nome</th>
                        <th>Status</th>
                        <th>Ações</th>
                        <th>Data de Criação</th>
                        <th>Ultima edição</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($storesTypes as $storeType)
                    <tr>
                        <td>{{ $storeType->name }}</td>
                        <td>{{ $storeType->status ? 'Ativado' : 'Desativado' }}</td>
                        <td>
                            <a href="{{ action('\Admin\StoresTypesController@getEdit', $storeType->id) }}" class="btn btn-default">Editar</a>
                            <a href="{{ action('\Admin\StoresTypesController@getDelete', $storeType->id) }}" class="btn btn-danger">Deletar</a>
                        </td>
                        <td>{{$storeType->created_at}}</td>
                        <td>{{$storeType->updated_at}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    @endif
@stop