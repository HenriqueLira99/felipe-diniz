@extends('admin.layout')

@section('content')
    <div class="page-header">
        <h1>Cadastre um novo Tipo de Imóvel <small></small></h1>
    </div>
    <div class="block-flat">
        <form action="{{ action('\Admin\StoresTypesController@postHandleCreate') }}" method="post" role="form">
            {{Form::token()}}
            <div class="form-group">
                <label>Nome</label>
                @foreach($errors->get('nome') as $message)
                    <div class="alert alert-warning">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <i class="fa fa-warning sign"></i><strong>Atenção!</strong> {{$message}}
                    </div>
                @endforeach
                <input type="text" placeholder="Digite o nome" required="required" class="form-control" name="nome">
            </div>
            <div class="form-group">
                <label>Status</label>
                @foreach($errors->get('status') as $message)
                    <div class="alert alert-warning">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <i class="fa fa-warning sign"></i><strong>Atenção!</strong> {{$message}}
                    </div>
                @endforeach
                <select class="form-control" name="status" required="required">
                    <option value="">Selecione...</option>
                    <option value="0">Desativado</option>
                    <option value="1">Ativado</option>
                </select>
            </div>
            <button class="btn btn-primary" type="submit">Cadastrar</button>
            <a href="{{action('\Admin\StoresTypesController@getIndex')}}"><button class="btn btn-default" type="button">Cancelar</button></a>
        </form>
    </div>
@stop