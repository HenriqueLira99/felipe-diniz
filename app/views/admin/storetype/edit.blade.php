@extends('admin.layout')

@section('content')
    <div class="page-header">
        <h1>Editar Tipo <small>edite o tipo cadastrado!</small></h1>
    </div>
    <div class="block-flat">
        <form action="{{action('\Admin\StoresTypesController@postHandleEdit')}}" method="post" role="form">
            {{Form::token()}}
            <input type="hidden" name="id" value="{{$storeType->id}}">

            <div class="form-group">
                <label for="title">Nome</label>
                <input type="text" class="form-control" name="name" value="{{$storeType->name}}" />
            </div>
            <div class="form-group">
                <label>Status</label>
                <select class="form-control" name="status">
                    <option value="0">Desativado</option>
                    <option value="1" {{$storeType->status ? 'selected' : ''}}>Ativado</option>
                </select>
            </div>
            <input type="submit" value="Editar" class="btn btn-primary" />
            <a href="{{ action('\Admin\StoresTypesController@getIndex') }}" class="btn btn-link">Cancelar</a>
        </form>
    </div>
@stop