{{-- call the layout.blade.php --}}
@extends('admin.layout')
{{-- add the content in layout --}}
@section('content')
    <div class="page-header">
        <h1>Capa <small>veja a imagem que está como capa do site.</small></h1>
    </div>
    <div class="block-flat">
        <img src="{{asset('images/banners/'.$banner->image)}}" class="img-responsive"/>
    </div>
@stop