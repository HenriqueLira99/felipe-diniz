@extends('admin.layout')
@section('before_head')
    <link rel="stylesheet" type="text/css" href="{{URL::asset('js/jasny.bootstrap/extend/css/jasny-bootstrap.min.css')}}">
@stop
@section('content')
    <div class="page-header">
        <h1>Edite <small>a capa atual do site!</small></h1>
    </div>
    <div class="block-flat">
        <form action="{{action('\Admin\BannersController@postHandleEdit')}}" method="post" role="form" enctype="multipart/form-data">
            {{Form::token()}}
            <input type="hidden" name="id" value="{{$banner->id}}">
            <div class="form-group">
                <label class="col-sm-12 control-label">Selecione a imagem</label>
                <div class="row">
                    <div class="col-sm-12 fileinput fileinput-new" data-provides="fileinput">
                        <span class="vertical-align-reference"></span>
                        <div class="vertical-align">
                            <span class="btn btn-primary btn-file"><span class="fileinput-new">Selecionar imagem</span>
                                <span class="fileinput-exists">Alterar</span><input type="file" name="image">
                            </span>
                            <a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Remover</a>
                        </div>
                        <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px; margin-left: 10px;"><p>Preview da capa</p></div>
                    </div>
                </div>
            </div>
            <input type="submit" value="Atualizar" class="btn btn-primary" />
            <a href="{{action('\Admin\BannersController@getIndex')}}" class="btn btn-link">Cancelar</a>
        </form>
    </div>
@stop
@section('scripts')
    <script type="text/javascript" src="{{URL::asset('js/jasny.bootstrap/extend/js/jasny-bootstrap.min.js')}}"></script>
@stop