@extends('admin.layout')

@section('content')
    <div class="page-header">
        <h1>Delete {{ $banner->title }} <small>Are you sure?</small></h1>
    </div>
    <form action="{{ action('BannersController@handleDelete') }}" method="post" role="form">
        {{Form::token()}}
        <input type="hidden" name="banner" value="{{ $banner->id }}" />
        <input type="submit" class="btn btn-danger" value="Yes" />
        <a href="{{ action('BannersController@index') }}" class="btn btn-default">No way!</a>
    </form>
@stop