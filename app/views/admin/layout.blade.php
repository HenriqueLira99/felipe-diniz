<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Felipe Diniz - Administração</title>
        @yield('before_head')
        @include('admin.html.head')
        @yield('head')
    </head>
    <body class="animated">
        <div id="cl-wrapper">
            @include('admin.html.left')
            @yield('admin.html.left')
            <div class="container">
                @yield('content')
                @include('admin.html.scripts')
                @yield('scripts')
            </div>
        </div>

        <script type="text/javascript">
            (function(){
                jQuery('document').ready(function(){
                    jQuery('.cl-vnavigation li.active').removeClass('active');
                    var $reference = jQuery('[href="{{ URL::current() }}"]');
                    if( !$reference.hasClass('parent') ) $reference.parents('li').first().addClass('active').parents('ul').first().show().parents('.parent').addClass('open');
                    else $reference.addClass('open').find('.sub-menu').first().show();

                    //jQuery('body').find('[href="http://localhost:8000/admin/stores-types/index"]').parents('ul').first().show().parents('.parent').addClass('active open')
                });
            })();
        </script>
    </body>
</html>