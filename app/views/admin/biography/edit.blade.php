@extends('admin.layout')
@section('head')
    <link href="{{URL::asset('js/bootstrap.wysihtml5/src/bootstrap-wysihtml5.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('js/bootstrap.summernote/dist/summernote.css')}}" />
@stop
@section('content')
    <div class="page-header">
        <h1>Conteúdo da página <small>HTML</small></h1>
    </div>

    <form action="{{action('\Admin\BiographiesController@postHandleEdit')}}" method="post" role="form">
        {{Form::token()}}
        <input type="hidden" name="id" value="{{$biography->id}}">
        <div class="row">
            <div class="col-md-12">
                <div class="block-flat">
                    <div class="header">
                        <h3>Editar conteúdo</h3>
                    </div>
                    <div class="content cms">
                        <textarea id="summernote" name="text">{{$biography->text}}</textarea>
                    </div>
                </div>
            </div>
        </div>
        <input type="submit" value="Atualizar" class="btn btn-primary" />
        <a href="{{action('\Admin\BiographiesController@getIndex')}}" class="btn btn-link">Cancelar</a>
    </form>
@stop
@section('scripts')
    <script type="text/javascript" src="{{URL::asset('js/bootstrap.summernote/dist/summernote.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('js/bootstrap.wysihtml5/lib/js/wysihtml5-0.3.0.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('js/bootstrap.wysihtml5/src/bootstrap-wysihtml5.js')}}"></script>

    <script type="text/javascript">
        jQuery(document).ready(function(){
            jQuery('#summernote').summernote();
        });
    </script>
@stop