{{-- call the layout.blade.php --}}
@extends('admin.layout')
{{-- add the content in layout --}}
@section('content')
    <div class="page-header">
        <h1>Biografia <small>veja como a página ficará abaixo.</small></h1>
    </div>
    <div class="block-flat cms">
        {{$biography->text}}
    </div>
@stop