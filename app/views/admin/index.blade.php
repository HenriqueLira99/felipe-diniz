{{-- call the layout.blade.php --}}
@extends('admin.layout')
{{-- add the content in layout --}}
@section('content')
    <div class="page-header">
        <h1>Imóveis <small>Veja a listagem de todos imóveis cadastrados!</small></h1>
    </div>

    <div class="panel panel-default">
        <div class="panel-body">
            <a href="{{ action('\Admin\StoresController@getCreate') }}" class="btn btn-primary">Cadastrar novo</a>
        </div>
    </div>

    @if ($stores->isEmpty())
        <p>Nenhum imóvel cadastrado! :(</p>
    @else
        <div class="block-flat">
            <table class="table no-border hover">
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Tipo</th>
                        <th>Status</th>
                        <th>Destaque na home</th>
                        <th>Data de criação</th>
                        <th>Última atualização</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($stores as $store)
                    <tr>
                        <td>{{ $store->name }}</td>
                        @if(count($store->type) > 0)
                            <td>{{ $store->type->name}}</td>
                        @else
                            <td>-</td>
                        @endif
                        <td>{{ $store->status ? 'Ativado' : 'Desativado' }}</td>
                        <td>{{ $store->featured ? 'Sim' : 'Não' }}</td>
                        <td>{{$store->created_at}}</td>
                        <td>{{$store->updated_at}}</td>
                        <td>
                            <a href="{{ action('\Admin\StoresController@getEdit', $store->id) }}" class="btn btn-default">Editar</a>
                            <a href="{{ action('\Admin\StoresController@getDelete', $store->id) }}" class="btn btn-danger">Deletar</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    @endif
@stop