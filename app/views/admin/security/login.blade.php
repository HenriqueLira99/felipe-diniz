@extends('admin.layout_noleft')
@section('content')

    @foreach($errors->all() as $message)
         <div class="alert alert-danger">
            {{$message}}
          </div>
    @endforeach

	<div class="login-container">
		<div class="middle-login">
			<div class="block-flat">
				<div class="header">							
					<h3 class="text-center">Felipe Diniz - Administração</h3>
				</div>
				<div>
					<form style="margin-bottom: 0px !important;" class="form-horizontal" method="POST" data-parsley-validate>
						<div class="content">
							<div class="form-group">
								<div class="col-sm-12">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-user"></i></span>
										<input type="text" placeholder="Usuário" name="username" id="username" class="form-control" required parsley-trigger="change">
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-12">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-lock"></i></span>
										<input type="password" placeholder="Senha" name="password" id="password" class="form-control" required>
									</div>
								</div>
							</div>
						</div>
						<div class="foot">
							<button class="btn btn-primary" data-dismiss="modal" type="submit">Entrar</button>
						</div>
						{{Form::token()}}
					</form>
				</div>
			</div>
			<div class="text-center out-links">
				<a href="http://18digital.com.br" target="_blank">&copy; 18digital</a>
			</div>
		</div> 
	</div>
@stop