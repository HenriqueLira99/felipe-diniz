@extends('admin.layout')
@section('before_head')
    <link rel="stylesheet" type="text/css" href="{{URL::asset('js/dropzone/css/dropzone.css')}}" />
@stop
@section('content')
    <div class="page-header">
        <h1>Editar Imóvel <small>edite o imóvel cadastrado!</small></h1>
    </div>
    <div class="block-flat">
        <form action="{{action('\Admin\StoresController@postHandleEdit')}}" method="post" role="form">
            <input type="hidden" name="id" value="{{$store->id}}">
            {{Form::token()}}
            <div class="form-group">
                <label>Nome</label>
                <input type="text" placeholder="Digite o nome" class="form-control" name="name" value="{{$store->name}}">
            </div>
            <div class="form-group">
                <label>Tipo</label>
                <select class="form-control" name="store_type_id">
                    <option value="">Selecione...</option>
                    @foreach($storesTypes as $storeType)
                        @if($storeType->id == $store->store_type_id)
                            <option value="{{$storeType->id}}" selected>{{$storeType->name}}</option>
                        @else
                            <option value="{{$storeType->id}}">{{$storeType->name}}</option>
                        @endif
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label>Status</label>
                <select class="form-control" name="status">
                    <option value="0">Desativado</option>
                    <option value="1" {{$store->status ? 'selected' : ''}}>Ativado</option>
                </select>
            </div>
            <div class="form-group">
                <label>Destaque na home?</label>
                <select class="form-control" name="featured_store">
                    <option value="0">Não</option>
                    <option value="1" {{$store->featured ? 'selected' : ''}}>Sim</option>
                </select>
            </div>
            <div class="form-group">
                <h3>Arraste as imagens do imóvel no bloco abaixo, ou clique nele.</h3>
                <div class="dropzone" id="store-dropzone">
                    <input type="hidden" name="id" value="{{$store->id}}">
                    {{Form::token()}}
                    @foreach($storeImages as $image)
                        <input type="file" name="file[]" value="{{ $image->path }}" data-featured="{{ $image->featured }}" data-sort-order="{{ $image->order }}" data-thumbnail="{{ URL::asset($image->path) }}" data-id="{{ $image->id }}" />
                    @endforeach
                </div>
            </div>
            <div class="form-group">
                <input type="submit" value="Editar" class="btn btn-primary" />
                <a href="{{ action('\Admin\StoresController@index') }}" class="btn btn-link">Cancelar</a>
            </div>
        </form>
    </div>
@stop
@section('scripts')
    <script type="text/javascript" src="{{URL::asset('js/dropzone/dropzone.js')}}"></script>

	<script type="text/javascript">
		Dropzone.autoDiscover = false;
		var $target = jQuery('#store-dropzone');
		var storeId = $target.find('input[name="id"]').val();
		var csrfToken = $target.find('input[name="_token"]').val();
        var fileIds = [];

		$target.dropzone({
			url: '{{action('\Admin\StoresController@postUpload')}}',
			previewsContainer: '#store-dropzone',
			uploadMultiple: true,
			parallelUploads: 50,
			maxFiles: 50,
			addRemoveLinks: true,
			init: function(){
				var that = this;
				$target.find('input[name="file[]"]').each(function(index, el){
				    fileIds.push({
				        id: el.getAttribute('data-id'),
				        filepath: el.getAttribute('data-thumbnail'),
				        order: el.getAttribute('data-sort-order'),
				        featured: el.getAttribute('data-featured') == "0" ? false : true
				    });

					var mockFile = {name: el.getAttribute('value'), size: 200, id: el.getAttribute('data-id')};
					that.emit('addedfile', mockFile);
					that.emit('thumbnail', mockFile, el.getAttribute('data-thumbnail'));

					jQuery(el).remove();
				});

				// Create sort order field
                jQuery('#store-dropzone .dz-size').each(function(index, element){
                    $element = jQuery(element);
                    $element.after("<div class=\"dz-sort-order\"><input type=\"text\" name=\"sort_order[]\" value=\"0\" maxlength=\"2\" placeholder=\"Posicionamento\" /></div><div class=\"dz-featured\"><p>Destaque?</p><input type=\"checkbox\" name=\"featured[]\" value=\"1\" /></div>");
                });

                // Update sort order names
                jQuery(fileIds).each(function(index, el){
                   $el = jQuery('.dz-details').has('[src="'+el.filepath+'"]');
                   $el.find('.dz-sort-order input').attr('name', 'sort_order['+el.id+']').val(el.order);
                   $el.find('.dz-featured input').attr('name', 'featured['+el.id+']').attr('checked', el.featured);
                });
			},
			sending: function(file, xhr, formData){
				formData.append('_token', csrfToken);
				formData.append('store_id', storeId);
			},
			successmultiple: function(file, response, xhr){
			    jQuery('#store-dropzone .dz-size').each(function(index, element){
                    $element = jQuery(element);
                    $element.after("<div class=\"dz-sort-order\"><input type=\"text\" name=\"sort_order[]\" value=\"0\" maxlength=\"2\" placeholder=\"Posicionamento\" /></div><div class=\"dz-featured\"><p>Destaque?</p><input type=\"checkbox\" name=\"featured[]\" value=\"1\" /></div>");
                });

			    jQuery(response).each(function(index, el){
			        jQuery('.dz-details').has('[alt="'+el.name+'"]').find('.dz-sort-order input').attr('name', 'sort_order['+el.id+']');
			    });
			},
			removedfile: function(file){
				jQuery.ajax({
					url: '{{action('\Admin\StoresController@postRemoveImage')}}',
					type: 'POST',
					data: {
						'_token': csrfToken,
						'store_id': storeId,
						'id': file.id
					},
					async: false,
					success: function(data){
						if(data == 'success') jQuery(file.previewElement).remove();
						else alert('Salve o slider antes de remover a imagem');
					}
				});
			}
		});
	</script>
@stop