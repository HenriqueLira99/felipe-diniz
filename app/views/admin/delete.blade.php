@extends('admin.layout')

@section('content')
    <div class="page-header">
        <h1>Você realmente quer deletar o " <i>{{$store->name}}</i> " <small>?</small></h1>
    </div>
    <form method="POST" role="form">
        {{Form::token()}}
        <input type="hidden" name="store" value="{{$store->id}}" />
        <input type="submit" class="btn btn-danger" value="Sim" />
        <a href="{{ action('\Admin\StoresController@index') }}" class="btn btn-default">Não, cheguei aqui sem querer!</a>
    </form>
@stop