<?php

class UsersTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run(){
		// Users
		DB::table('users')->insert([
			[
				'username' => 'admin',
				'password' => Hash::make('digital18'),
				'status' => 1,
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'username' => 'developer',
				'password' => Hash::make('digital18'),
				'status' => 1,
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			]
		]);

        DB::table('stores_types')->insert([
            [
                'status' => 1,
                'name' => 'Comercial',
                'alias' => 'comercial',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'status' => 1,
                'name' => 'Residencial',
                'alias' => 'residencial',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'status' => 1,
                'name' => 'Mostras',
                'alias' => 'mostras',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]
        ]);

        DB::table('cms')->insert([
            [
                'page_name' => 'Bio',
                'alias' => 'bio',
                'text' => '',
                'status' => 1,
                'code' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'page_name' => 'Contato',
                'alias' => 'contato',
                'text' => '',
                'status' => 1,
                'code' => 2,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]
        ]);

        DB::table('banners')->insert([
            [
                'image' => 'capa.jpg',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]
        ]);
	}

}
