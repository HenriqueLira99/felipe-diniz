<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBanners extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        // Create the biography entry
        if(!Schema::hasTable('banners')) {
            Schema::create('banners', function ($table) {
                $table->increments('id');
                $table->string('image', 300);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        // Drop the bass (8'
        if(Schema::hasTable('banners')) {
            Schema::drop('banners');
        }
    }

}
