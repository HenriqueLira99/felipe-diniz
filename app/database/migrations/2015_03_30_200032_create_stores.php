<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStores extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up(){
		// Create the stores
		if(!Schema::hasTable('stores')) {
            Schema::create('stores', function ($table) {
                $table->increments('id');
                $table->string('name', 350);
                $table->string('alias', 350);
                $table->integer('store_type_id')->nullable()->unsigned();
                $table->tinyInteger('status')->nullable()->default('0');
                $table->tinyInteger('featured')->nullable()->default('0');
                $table->timestamps();
            });
        }

        if(!Schema::hasTable('stores_types')) {
            Schema::create('stores_types', function ($table) {
                $table->increments('id');
                $table->tinyInteger('status')->nullable()->default('0');
                $table->string('name', 350);
                $table->string('alias', 350);
                $table->timestamps();
            });
        }

        if(!Schema::hasTable('stores_images')) {
            Schema::create('stores_images', function ($table) {
                $table->increments('id');
                $table->integer('store_id')->unsigned();
                $table->integer('order')->nullable();
                $table->tinyInteger('featured')->nullable()->default('0');
                $table->string('path', 350);
                $table->timestamps();
            });
        }

        Schema::table('stores', function($table){
            $table->foreign('store_type_id')->references('id')->on('stores_types')->onDelete('set null')->onUpdate('cascade');
        });

        Schema::table('stores_images', function($table){
            $table->foreign('store_id')->references('id')->on('stores')->onUpdate('cascade')->onDelete('cascade');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down(){
		// Drop the bass (8'
        if(Schema::hasTable('stores')) {
            Schema::drop('stores');
        }

        if(Schema::hasTable('stores_types')) {
            Schema::drop('stores_types');
        }

        if(Schema::hasTable('stores_images')) {
            Schema::drop('stores_images');
        }
	}

}
