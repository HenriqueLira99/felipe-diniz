<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up(){
        // Users
        if( !Schema::hasTable('users') ){
            Schema::create('users', function($table){
                $table->increments('id');
                $table->string('username', 150);
                $table->string('password', 70);
                $table->boolean('status')->default(0)->nullable();
                $table->rememberToken();
                $table->timestamps();
            });
        }
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down(){
        if( Schema::hasTable('users') ){
            Schema::drop('users');
        }
	}

}
