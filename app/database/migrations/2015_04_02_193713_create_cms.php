<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCms extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
    public function up(){
        // Create the biography entry
        if(!Schema::hasTable('cms')) {
            Schema::create('cms', function ($table) {
                $table->increments('id');
                $table->string('page_name', 350);
				$table->string('alias', 350);
                $table->text('text');
                $table->tinyInteger('status')->nullable()->default('0');
                $table->tinyInteger('code')->nullable()->default('0');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        // Drop the bass (8'
        if(Schema::hasTable('cms')) {
            Schema::drop('cms');
        }
    }

}
