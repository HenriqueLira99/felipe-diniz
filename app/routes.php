<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// Bind route parameters.
//  Route::model('game', 'Game');

// Admin Route Group

Route::group([
        'prefix' => 'admin',
        'namespace' => '\Admin',
    ],
    function(){
        // For unlogged users
        Route::group(['before' => ['auth.admin.basic']], function(){
            Route::get('login', 'LoginController@getLogin');
            Route::post('login', 'LoginController@postLogin');
        });

        // Confirm logged user
        Route::group(['before' => ['auth.admin']], function(){
            // Show pages.
            Route::get('/', 'StoresController@index');
            Route::controller('stores', 'StoresController');
            Route::controller('stores-types', 'StoresTypesController');
            Route::controller('banners', 'BannersController');
            Route::controller('contacts', 'ContactsController');
            Route::controller('biographies', 'BiographiesController');
            Route::get('logout', 'LoginController@getLogout');
        });
    }
);

// Frontend Route
Route::group([
        'prefix' => '/',
        'namespace' => '\Frontend',
    ],
    function(){
        // Show pages.
        Route::get('/', 'StoresController@index');
        Route::get('/biografia', 'CmsController@getBiografia');
        Route::get('/contato', 'CmsController@getContato');
        Route::get('/{storetype}', 'StoresTypesController@getIndex');
        Route::get('/imovel/{store_alias}/{store_id}', 'StoresController@getImovel');
    }
);