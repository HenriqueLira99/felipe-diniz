/**
 * 18digital
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GNU General Public License (GPL 3.0)
 * that is bundled with this package in the file LICENSE_GPL.txt
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/gpl-3.0.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to suporte@18digital.com.br so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * You can edit, copy, distribute and change this file, however, no information
 * about it's author, company, owner or any legal information can be changed,
 * erase or edited in no circumstances.
 *
 * @application	After image load
 * @package    	afterimageload
 * @author	   	Douglas Leandro Costa de Carvalho
 * @coauthor	Samir J.M. Araujo
 * @revisor		Samir J.M. Araujo
 * @authorEmail	douglas.leandro@18digital.com.br, samir.araujo@18digital.com.br
 * @company	   	18digital
 * @owner	   	18digital
 * @copyright  	Copyright (c) 2015 18digital (http://18digital.com.br)
 * @version	   	1.1.0
 * @license    	GPL-3.0  GNU General Public License (GPL 3.0)
 * @licenseUrl 	http://opensource.org/licenses/gpl-3.0.html
 */
 
/**
 * Last improvements 
 * @revisor Samir J.M. Araujo
 * Add fadein effect on all images load
 */
function AfterImagesLoad(){
	var config = {
		container: ''
	};

	/**
	 * Init function
	 * 
	 * @return void
	 */
	this.init = function(){
		// Hide images before load
		hideImages();

		// Create temporary container
		createContainer();

		// Move images into temporary container
		moveImages();
	};

	/**
	 * Hide images from main container
	 * 
	 * @return void
	 */
	function hideImages(){
		jQuery('.box-holder img').hide();
	}

	/**
	 * Create temporary container to images load
	 * and set config.container variable as reference
	 * 
	 * @return void
	 */
	function createContainer(){
		jQuery('body').prepend('<div id="images-loader" style="position: fixed; top: -9999px; left: -99999px;"></div>');
		config.container = jQuery('#images-loader');
	}

	/**
	 * Remove current temporary container
	 * 
	 * @return void
	 */
	function removeContainer(){
		config.container.remove();
	}

	/**
	 * Move images into current temporary container
	 * 
	 * @return void
	 */
	function moveImages(){
		jQuery('.box-holder img').each(function(index, el){
			var id = createId();
			config.container.append('<img src="'+el.src+'" id="'+id+'" />');
			observeImage(id);
		});
	}

	/**
	 * Create random ID
	 * 
	 * @return string
	 */
	function createId(){
		return Math.random().toString().split('.')[1];
	}

	/**
	 * Observe image load event and trigger methods upon "complete" status.
	 * Also adds an timeout for smoother image drawing
	 * 
	 * @return void
	 */
	function observeImage(id){
		jQuery('#'+id).load(function(){
			var obj = this;
			setTimeout(function(){
				jQuery('[src="'+obj.src+'"]').fadeIn(600);
				jQuery(obj).remove();
				checkContainer();
			}, 800);
		});
	}

	/**
	 * Check if container is empty, meaning all images were loaded.
	 * If it's empty, remove current temporary container and start
	 * Masonry
	 * 
	 * @return void
	 */
	function checkContainer(){
		if( config.container.find('img').length <= 0 ){
			removeContainer();
			startMasonry();
		}
	}

	/**
	 * Instantiate Masonry
	 * 
	 * @return void
	 */
	function startMasonry(){
		var $container = jQuery('.box-holder');
		$container.imagesLoaded(function() {
			$container.masonry({
				columnWidth: '.item',
				itemSelector: '.item'
			})
		});
	}
}

$(document).ready(function() {
	// Smoother images loading
	var observeImages = new AfterImagesLoad;
	observeImages.init();
});