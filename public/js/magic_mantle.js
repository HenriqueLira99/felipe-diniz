/*Thanks to Luke Archer*/
/*
 Website   MAAD
 Version   2.0
 Design    Luke Archer
 Code      Luke Archer
 */

// NATIVE CSS
var nativeTransition,
    nativeTransform,
    nativeTransitionEnd,

    nativeTransition =
        'Transition' in document.body.style && 'transition' ||
        'WebkitTransition' in document.body.style && '-webkit-transition' ||
        'MozTransition' in document.body.style && '-moz-transition' ||
        'msTransition' in document.body.style && '-ms-transition' ||
        'OTransition' in document.body.style && '-o-transition';

nativeTransform =
    'Transform' in document.body.style && 'transform' ||
    'WebkitTransform' in document.body.style && '-webkit-transform' ||
    'MozTransform' in document.body.style && '-moz-transform' ||
    'msTransform' in document.body.style && '-ms-transform' ||
    'OTransform' in document.body.style && '-o-transform';

nativeTransitionEnd =
    'Transition' in document.body.style && 'transitionend' ||
    'WebkitTransition' in document.body.style && 'webkitTransitionEnd' ||
    'MozTransition' in document.body.style && 'transitionend' ||
    'OTransition' in document.body.style && 'oTransitionEnd';

jQuery(document).ready(function($) {
    function overlayImage() {
        var im1 = $('#overLayImage img');
        var doc_width = $(window).width();
        var doc_height = $(window).height();
        var image_width = im1.width();
        var image_height = im1.height();
        var image_ratio = image_width/image_height;
        var new_height = Math.round(doc_width/image_ratio);

        im1.width(doc_width);
        im1.height(new_height);

        if(new_height<doc_height){
            new_height = doc_height;
            new_width = Math.round(new_height*image_ratio);

            im1.width(new_width);
            im1.height(new_height);

            var width_offset = Math.round((new_width-doc_width)/2);
            im1.css("left","-"+width_offset+"px");
        } else {
            im1.css("left", 0);
        }

        if(new_height>doc_height){
            var height_offset = Math.round((new_height-doc_height)/2);
            im1.css("top","-"+height_offset+"px");
        } else {
            im1.css("top", 0);
        }
    };

    overlayImage();

    // OVERLAY
    jQuery('#overLayImage img').show();

    var over = jQuery.cookie('overlay_cookie');
    var widthWindow = jQuery(window).width();
    var widthWindowPx =  "-" + widthWindow + "px";
    var $overlay = jQuery('#overLay');

    console.log(over);

    if(over == 'off') {
        $overlay.css(nativeTransform, 'translateX(-100%)');
        $overlay.addClass('hide-mobile');

    } else {
        $overlay.css(nativeTransform, 'translateX(-20px)');
    }

    $(document).on({
        mouseenter: function () {
            over = jQuery.cookie('overlay_cookie');
            if(over == 'off') {
                var owidth = $(window).width()-20;
                $overlay.addClass('overlayClickTransition');
                $overlay[0].offsetHeight;
                $overlay.css(nativeTransform, 'translateX(-'+owidth+'px)');
                $overlay.one(nativeTransitionEnd, function(){
                    $overlay.removeClass('overlayClickTransition');
                    $overlay[0].offsetHeight;
                });
				$overlay.addClass('hide-mobile');
            }
        },

        mouseleave: function () {
            over = jQuery.cookie('overlay_cookie');
            if(over == 'off') {
                $overlay.addClass('overlayClickTransition');
                $overlay[0].offsetHeight;
                $overlay.css(nativeTransform, 'translateX(-100%)');
                $overlay.one(nativeTransitionEnd, function(){
                    $overlay.removeClass('overlayClickTransition');
                    $overlay[0].offsetHeight;
                    /*jQuery('#overLay').remove();
                     jQuery('#overLayClick').remove();*/
                });
				$overlay.addClass('hide-mobile');
            }
        }
    }, '#overLayClick');

    $('body').css('opacity',1);

    jQuery('#overLayBar, #phrase, #overLayClick').click(function() {
        over = jQuery.cookie('overlay_cookie');

        if(over == 'off'){
            jQuery.cookie('overlay_cookie', 'on');
            $overlay.addClass('overlayTransition');
            $overlay[0].offsetHeight;
            $overlay.css(nativeTransform, 'translateX(-20px)');
            $overlay.one(nativeTransitionEnd, function(){
                $overlay.removeClass('overlayTransition');
                $overlay[0].offsetHeight;
            });
			$overlay.addClass('hide-mobile');
        } else {
            jQuery.cookie('overlay_cookie', 'off');
            $overlay.addClass('overlayTransition');
            $overlay[0].offsetHeight;
            $overlay.css(nativeTransform, 'translateX(-100%)');
            $overlay.one(nativeTransitionEnd, function(){
                jQuery('#overLayImage img').show();
                //overlayImageNew();
                $overlay.removeClass('overlayTransition');
                $overlay[0].offsetHeight;
            });
			$overlay.addClass('hide-mobile');
        }
    });
});